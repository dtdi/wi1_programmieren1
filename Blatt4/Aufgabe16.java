public class Aufgabe16 {
	public static void main(String args[]) {
		stars3_10();
		System.out.println();
		stars(5, 12);
	}
	public static void stars(int h, int b) {
		for(int i=0; i<h;i++) {
			for(int j=0; j<b; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
	public static void stars3_10() {
		stars(3, 10);
	}
}