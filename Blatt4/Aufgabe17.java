 public class Aufgabe17 {
	public static void main(String args[]) {
		System.out.println(fak(4));
	}
	public static int fak(int i) {
		int fak=0;
		if(i==0) {
			return 1;
		}
		for(int c=1;c<=i;c++) {
			fak+=c;
		}
		return fak;
	}
 }