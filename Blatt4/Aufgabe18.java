public class Aufgabe18 {
	public static void main(String args[]) {
		System.out.println(wt2String(10));
		System.out.println(monat2String(20));
	}

	public static String wt2String(int wt) {
		switch(wt) {
			case 0: return "Sonntag";
			case 1: return "Montag";
			case 2: return "Dienstag";
			case 3: return "Mittwoch";
			case 4: return "Donnerstag";
			case 5: return "Freitag";
			case 6: return "Samstag";
			default: return "Error";
		}
	}
	
	public static String monat2String(int month) {
		switch(month) {
			case 1: return "Januar";
			case 2: return "Februar";
			case 3: return "März";
			case 4: return "April";
			case 5: return "Mai";
			case 6: return "Juni";
			case 7: return "Juli";
			case 8: return "August";
			case 9: return "September";
			case 10: return "Oktober";
			case 11: return "November";
			case 12: return "Dezember";
			default: return "Error";
		}
	}
}