public class Aufgabe20 {
	public static void main(String args[]) {
		rect(5,5);
		tri(5,5);
		diamond(5,5);
	}

	public static void rect(int height, int width) {
		paintStars(0, width);
		for(int i=0;i<height-2;i++) {
			paintStarBlanksStar(0,width-2);
		}
		paintStars(0, width);
	}

	public static void tri(int height, int width) {
		int c=0;
		for(int i=1;i<=height-1;i++) {
			paintStarBlanksStar(width-i,2*c++);
		}
		paintStars(0, width*2);
	}

	public static void diamond(int height, int width) {
		int c=0;
		for(int i=1;i<=height-1;i++) {
			paintStarBlanksStar(width-i,2*c++);
		}
		c=width-2;
		for(int i=1;i<=height-1;i++) {
			paintStarBlanksStar(i,2*c--);
		}
	}
	
	public static void paintStars(int offset, int anz) {
		for(int i=0; i<offset; i++) {
			System.out.print(" ");
		}
		
		for(int i=0; i<anz; i++) {
			System.out.print("*");
		}
		System.out.println();
	}
	
	public static void paintStarBlanksStar(int offset, int anz) {
		for(int i=0; i<offset; i++) {
			System.out.print(" ");
		}
		System.out.print("*");
		for(int i=0; i<anz; i++) {
			System.out.print(" ");
		}
		System.out.println("*");
	}
}