public class Aufgabe19 {
	public static void main(String args[]) {
		int day = getNumBetween(1, 31, "Tag");
		int month = getNumBetween(1, 12, "Monat");
		int year = getNumBetween(1000, 9999, "Jahr");
		
		int wd = calcDayOfWeek(day, month, year);
		printDay(wd);
	}
	
	public static int calcDayOfWeek(int d, int m, int y) {
		switch(m) {
			case 1: m=13; y--; break;
			case 2: m=14; y--; break;
		}
		int c = y/100;
		y = y%100;
		int w = (d + (26*(m+1))/10 + (5*y)/4 + c/4 + (5*c)-1) % 7;
		return w;
	}
	
	public static void printDay(int w) {
		switch(w) {
			case 0: System.out.println("Sonntag"); break;
			case 1: System.out.println("Montag"); break;
			case 2: System.out.println("Dienstag"); break;
			case 3: System.out.println("Mittwoch"); break;
			case 4: System.out.println("Donnerstag"); break;
			case 5: System.out.println("Freitag"); break;
			case 6: System.out.println("Samstag"); break;
			default: System.out.println("Error"); break;
		}
	}
	
	public static int getNumBetween(int low, int high, String text) {
		int val = 0;

		do {
			val = Integer.parseInt(javax.swing.JOptionPane.showInputDialog(null, text));
		} while(val > high || val < low);
		return val;
	}
}