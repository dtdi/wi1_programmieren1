 public class Aufgabe21 {
	public static void main(String args[]) {
		System.out.println(sum(3,5));
	}
	
	public static int sum(int a, int b) {
		int res=0;
		if(b<=a) {
			res = b;
			b=a;
			a=res;
			res=0;
		}
		for(; a++ <b; a++) {
			res += a;
		}
		return res;
	}
 }