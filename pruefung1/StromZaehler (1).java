/* Teilnahmecode: dh104 Namen: Tobias Fehrer */

public class StromZaehler {
	String typ;
	int bj;
	int zStand;
	int lastSt;

	public StromZaehler(String typ, int bj) {
		this.typ = typ;
		this.bj = bj;
		// lastSt is being initialized implicit
		this.zStand = 433;
	}

	public void addEn(int kwh) {
		this.zStand += kwh;
	}

	public int ablesen() {
		this.lastSt = this.zStand;
		return(this.lastSt);
	}

	public double getCost() {
		return ((zStand - lastSt) * 45.00) / 100.00;
	}

}