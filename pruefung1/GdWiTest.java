public class GdWiTest {
	public static void main(String args[]) {
		double zahl = 10.2;
		System.out.println(zahl);
		int ganz = (int) zahl;
		int rat = 0;
		zahl -= ganz;
		System.out.println(zahl);

		while(zahl - rat > 0) {
			zahl *= 10;
			rat = (int) zahl;
		}
		System.out.println(toBin(ganz) + "." + toBin(rat));

	}
	public static String toBin(int zahl) {
		String bin = "";
		while(zahl > 1) {
			zahl /= 2;
			if(zahl % 2 == 1) {
				bin = "1" + bin;
				zahl -= 1;
			} else {
				bin = "0" + bin;
			}
		}
		return bin;
	}
}