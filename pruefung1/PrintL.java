/* Teilnahmecode: dh104 Namen: Tobias Fehrer */

public class PrintL {
	public static void main(String args[]) {
		prL(3, 5, 4);
		System.out.println(anzTeiler(12,2));
		System.out.println(xChange("825 Fehre8", '8'));
	}

	/**
	 * displays a "L", indented by the value of l,
	 * with given height h and width b;
	 */
	public static void prL(int indent, int h, int b) {
		for(int j=0;j<h;j++) {
			for(int i=0;i<indent;i++) {
				System.out.print(" ");
			}
			if(j!=h-1) {
				System.out.print("*\n");
			} else {
				for(int i=0;i<b;i++) {
					System.out.print("*");
				}
				System.out.println();
			}
		}
	}

	/**
	 * displays, how often n may be devided by m.
	 */
	public static int anzTeiler(int n, int m) {
		int cnt=0;
		while(n%m==0) {
			n = n/m;
			cnt++;
		}

		return cnt;
	}

	/**
	 * replaces x with a given String in s.
	 */
	public static String xChange(String s, char x) {
		String repl = "@825@";
		System.out.println(s.length());
		for(int i=0; i<s.length(); i++) {
			if(s.charAt(i) == x) {
				s = replace(s,repl,i);
				i+=repl.length();
			}
			System.out.println(i);
		}
		return s;
	}

	/**
	 *	Helper function to replace a String 
	 */ 
	public static String replace(String s, String repl, int index) {
    	String sBegin = s.substring(0,index);
    	String sEnd = s.substring(index+1);
    	return sBegin + repl + sEnd;
	}
}