public class MyDouble {
	int mantisse;
	int exponent;
	final int BASIS = 10;
	MyDouble(int mantisse, int exponent) {
		this.mantisse = mantisse;
		this.exponent = exponent;
	}
	MyDouble(double zahl) {
		int exp = 0;
		while(zahl < 1.0) {
			exp--;
			zahl = zahl * BASIS;
		}
		while(zahl > 10.0) {
			exp++;
			zahl = zahl / BASIS;
		}
		this.exponent = exp;
		this.mantisse = (int) zahl;
		System.out.println(exp);
	}

	public String toString() {
		double mul= Math.pow(BASIS, this.exponent);
		System.out.println(this.mantisse);
		System.out.println(mul);
		return this.mantisse * mul + "";
	}

	public static MyDouble add(MyDouble d1, MyDouble d2) {
		return d1;
	}

	public static MyDouble add(MyDouble d) {
		return d;
	}

	public MyDouble addResult(MyDouble d) {
		return d;
	}

	public void fillArray(MyDouble a[]) {

	}
}
