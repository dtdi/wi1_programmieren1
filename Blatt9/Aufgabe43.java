import static jsTools.Input.*;
import static jsTools.Graph.*;

public class Aufgabe43 {
	public static void main(String args[]) {
		addRect(0,0,680,680, "white");
		Auto auto1 = new Auto("VW", 30, 30, 300, 1999, "blue");
		auto1.paint();

		Auto auto2 = new Auto("Mazda", 500, 500, 300, 1999, "green");
		auto2.paint();
		sleep(100);

		auto2.driveTo(100, 100, 20);
		sleep(100);
		auto1.driveTo(340, 100, 10);
	}
}

class Auto {
	private String hersteller;
	private int posX;
	private int posY;
	private double kmStand;
	private int baujahr;
	private int[] parts = new int[4];
	private int[][] offsets = new int[4][2];
	private String color = "black";
	private int width = 40;
	private int height = 10;

	public Auto() {
		this.readValues();
	}

	public Auto(String hst, int x, int y, int km, int bj, String color) {
		this.init(hst, x, y, km, bj, color);
	}

	public void init(String hst, int x, int y, int km, int bj, String color) {
		this.hersteller = hst;
		this.posX = x;
		this.posY = y;
		this.kmStand = (double) km;
		this.baujahr = bj;
		this.color = color;
	}

	public void readValues() {
		this.hersteller = readString("Hersteller: ");
		this.posX = readInt("Pos X: ");
		this.posY = readInt("Pos Y: ");
		this.kmStand = readInt("kmStand: ");
		this.baujahr = readInt("Baujahr: ");
	}

	public void driveTo(int xNeu, int yNeu) {
		this.kmStand += Math.sqrt(
			Math.pow(this.posX - xNeu, 2) + 
			Math.pow(this.posY - yNeu, 2));
		this.posX = xNeu;
		this.posY = yNeu;
		this.zeichneNeu();
	}

	public void driveTo(int xNeu, int yNeu, int speed) {
		this.kmStand += Math.sqrt(
			Math.pow(this.posX - xNeu, 2) + 
			Math.pow(this.posY - yNeu, 2));
		int stepX = (xNeu - this.posX) / speed;
		int stepY = (yNeu - this.posY) / speed;
		System.out.println(stepX + " " + stepY);
		while(this.posX != xNeu) {
			this.posX += stepX;
			this.posY += stepY;
			this.zeichneNeu();
			sleep(20);
		}	
		this.posX = xNeu;
		this.posY = yNeu;
		this.zeichneNeu();
	}

	public void print() {
		System.out.println(
			"Hersteller:\t" + this.hersteller 
			+ "\nposX:\t" + this.posX
			+ "\nposY:\t" + this.posY
			+ "\nkmStand:\t" + this.kmStand
			+ "\nBaujahr:\t" + this.baujahr
		);
	}

	public void paint() {
		this.offsets[1][0] = this.width/4;
		this.offsets[1][1] = 0- this.height;
		this.offsets[2][1] = 5;
		this.offsets[3][0] = 25;
		this.offsets[3][1] = 5;


		this.parts[0] = addRect(0, 0, width, height, this.color);
		this.parts[1] = addRect(0, 0, width/2, height, this.color);
		this.parts[2] = addCircle(0, 0, 12, "gray");
		this.parts[3] = addCircle(0, 0, 12, "gray");
		this.zeichneNeu();
	}

	public void zeichneNeu() {
		for(int i=0; i<this.parts.length;i++) {
			setPos(this.parts[i], this.posX + this.offsets[i][0], this.posY + this.offsets[i][1]);
		}
		paintNew();
	}
}