class RatZahl{
  int z;
  int n;
  RatZahl (){}
  RatZahl (int z0, int n0){
    z=z0; n=n0;
  }
  void print(){
    System.out.println(z+"/"+n);
  }

  
  RatZahl mul (RatZahl r1){            // mul Variante I
    RatZahl erg;
    erg=new RatZahl();
    erg.z=r1.z*z;
    erg.n=r1.n*n;
    return erg;
  }

  RatZahl mul1 (RatZahl r1){           // mul Variante II
    return new RatZahl(r1.z*z,r1.n*n);
  }

  void mul (RatZahl r1, RatZahl r2){  // mul Variante III
    z=r1.z*r2.z;
    n=r1.n*r2.n;
  }

  void mul1 (RatZahl e, RatZahl r1){  // mul Variante IV
    e=new RatZahl();
    e.z=r1.z*z;
    e.n=r1.n*n;
  }
} 
public class RatMult{
  public static void main(String [] arg){
    RatZahl a=new RatZahl(1,2);  // also 1/2
    RatZahl b=new RatZahl(3,5);  // also 3/5
    
    RatZahl erg1=a.mul(b);
    erg1.print();

    RatZahl erg2=a.mul1(b);
    erg2.print();

    RatZahl erg3=new RatZahl();
    erg3.mul(a,b);
    erg3.print();

    RatZahl erg4=new RatZahl();
    a.mul1(erg4,b);
    erg4.print();
  }
}

