import static jsTools.Graph.*;

public class RunningBallWhile {
	public static void main(String args[]) {
		int px=10,
			kreis;
		int py=500;

		addRect(10,10,680,680, "gray");
		kreis = addCircleBorder(px, py, 30, "blue");

		for(int i=0; i<670;i+=20) {
			sleep(20);
			setPos(kreis, i, py);
			paintNew();
		}
		for(int i=670; i>0;i-=20) {
			sleep(20);
			py-=20;
			setPos(kreis, i, py);
			paintNew();
		}
	}
}