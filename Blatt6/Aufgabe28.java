import static jsTools.Graph.*;

public class Aufgabe28 {
	public static void main(String args[]) {
		int px=10,
			kreis;
		final int PY=500;

		addRect(10,10,680,680, "white");
		kreis = addCircleBorder(px, PY, 30, "blue");

		for(int i=0; i<500;i++) {
			sleep(20);
			px+=2;
			setPos(kreis, px, PY);
			paintNew();
		}
	}
}