import static jsTools.Graph.*;
import javax.swing.*;

public class Aufgabe29 {
	public static void main(String args[]) {
		addRect(0,0,800,800, "gray");
		paintNew();

		int[] rect1 = drawRect("blue");
		int[] rect2 = drawRect("red");

		// check for vertical collison
		boolean bVertical = false;
		if(rect1[0] < rect2[0] & rect2[0] < rect1[2]) {
			bVertical = true;
		}
		if(rect1[0] < rect2[2] & rect2[2] < rect1[2]) {
			bVertical = true;
		}
		// check for horizontal collison
		boolean bHorizontal = false;
		if(rect1[1] < rect2[1] & rect2[1] < rect1[3]) {
			bHorizontal = true;
		}
		if(rect1[1] < rect2[3] & rect2[3] < rect1[3]){
			bHorizontal = true;
		}
		boolean bContains = false;
		if(!bHorizontal & !bVertical) {
			if(rect2[0] < rect1[0] & rect1[0] < rect2[2]) {
				if(rect2[1] < rect1[1] & rect1[1] < rect2[3]) {
					bContains = true;
				}
			}
		}

		if((bHorizontal & bVertical) || bContains) {
			System.out.println("Collision");
		} 
	}


	/**
	 * Creates rectangles and returns them in ltrb (left, top, right, bottom - border)
	 */
	public static int[] drawRect(String color) {
		String[] messages = {
			"X eingeben",
			"Y eingeben",
			"Breite eingeben",
			"Hoehe eingeben" };
		int[] dim = new int[4];
		String rect = "Rechteck " + color + ": ";
		for(int i=0; i<2;i++) {
			do {
				dim[i] = Integer.parseInt(JOptionPane.showInputDialog(null, 
					rect + messages[i]));
			}
			while(dim[i] < 0 || dim[i] > 799);
		}
		for(int i=2; i<4;i++) {
			do {
				dim[i] = Integer.parseInt(JOptionPane.showInputDialog(null, rect + messages[i]));
			}
			while(dim[i]+dim[i-2] < 0 || dim[i]+dim[i-2] > 800);
		}
		addRect(dim[0], dim[1], dim[2], dim[3], color);
		paintNew();

		// Convert to ltrb

		dim[2] = dim[0] + dim[2];
		dim[3] = dim[1] + dim[3];

		return dim;
	}
}