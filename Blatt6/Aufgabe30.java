public class Aufgabe30 {
	public static void main(String args[]) {
		// Test 1;
		String s="XYZ";
		System.out.println(decryptText(cryptText(s, 5), 5));

		// Test 2;
		s = "INJXJW YJCY NXY OJYEY JSYXHMQZJXXJQY";
		System.out.println(decryptText(s, 5));

		// Test 3: 
		s = "/qrrcre/jng.ugzy";
		for(int i = 1; i<26; i++) {
			System.out.println(decryptText(s, i));
		}

	}
	public static char crypt(char c, int n) {
		c = capitalize(c);
		if(c >= 65 & c <=90) {
			c = (char) (c+n);
			if(c%65>26) {
				c = (char) (c-26);
			}
		}
		return c;
	}
	public static char decrypt(char c, int n) {
		c = capitalize(c);
		if(c >= 65 & c <=90) {
			c = (char) (c-n);
			if(c%65>26) {
				c = (char) (c+26);
			}
		}
		return c;
	}
	public static String cryptText(String s, int n) {
		String buf="";
		for(int i = 0; i<s.length(); i++ ) {
			buf+=crypt(s.charAt(i), n);
		}
		return buf;
	}
	public static String decryptText(String s, int n) {
		String buf="";
		for(int i = 0; i<s.length(); i++ ) {
			buf+=decrypt(s.charAt(i), n);
		}
		return buf;
	}
	public static char capitalize(char c) {
		if(c >= 97 & c <= 123) {
			c = (char) (c - 32);
		}
		return c;
	}
}