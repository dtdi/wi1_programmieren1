public class Aufgabe31 {
	public static void main(String args[]) {
		testArray();
	}

	public static void print(char arr[]) {
		for(int i=0;i<arr.length; i++) {
			System.out.print(arr[i]);
		}
		System.out.println();
	}
	public static void printReverse(char arr[]) {
		for(int i=arr.length; i>=0; i--) {
			System.out.print(arr[i]);
		}
		System.out.println();
	} 
	public static boolean contains(char arr[], char x) {
		for(int i=0;i<arr.length; i++) {
			if(arr[i] == x) 
				return true;
		}
		return false;
	}
	public static int countx(char arr[ ], char x) {
		int cnt = 0;
		for(int i=0;i<arr.length; i++) {
			if(arr[i] == x) 
				cnt++;
		}
		return cnt;
	} 
	public static boolean equals(char arr[ ], char barr[ ]) {
		if(arr.length != barr.length)
			return false;
		for(int i=0;i<arr.length; i++) {
			if(arr[i] != barr[i]) 
				return false;
		}
		return true;
	} 
	public static char[] replace(char arr[ ], char x, char y) {
		for(int i=0;i<arr.length; i++) {
			if(arr[i] == x) 
				arr[i] = y;
		}
		return arr;
	}

	public static void testArray(){ 
		// so kann man Strings in char - Arrays wandeln: 
		char arr[]="Ein Text zum Test".toCharArray();
		if (countx(arr,'T') == 2 ){
			System.out.println("OK");
		} else
			System.out.println("countx(arr,'T') ="+
				countx(arr,'T') );

		if (contains(arr,'T') == true ){
			System.out.println("OK");
		} else
			System.out.println("contains(arr,'T') ="+
				contains(arr,'T') );

		if (equals(arr,arr)){
			System.out.println("OK");
		} else
			System.out.println("equals(arr,arr) ="+
				equals(arr, arr) );

		char tarr[]="Ein Jext zum Jest".toCharArray();
		if (equals(replace(arr,'T', 'J'),tarr) ) {
			System.out.println("OK");
		} else
			System.out.println("replace(arr,'T', 'J') ="+
				replace(arr,'T', 'J') );
	}
}