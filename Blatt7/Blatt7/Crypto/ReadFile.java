import static jsTools.Input.*;

class ReadFile {
  public static void main(String args[]){
    String ln;
    System.out.println();
    
    // Ein File oeffnen, zum Lesen vorbereiten
    fileReadOpen("Substitution.tab");
    //eine Zeile lesen:
    ln=fileReadLine();
    System.out.println(ln);
    // Zweite Zeile Lesen:
    ln=fileReadLine();
    System.out.println(ln);
    
    fileReadOpen("Geheim.txt");
    ln=fileReadLine();
    while (ln != null) {
      System.out.println(ln);
      ln=fileReadLine();
    }
    
    
  }
}
