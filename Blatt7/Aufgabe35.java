public class Aufgabe35 {
	public static void main(String args[]) {
		char[] key = "UFLPWDRASJMCONQYBVTEXHZKGI".toCharArray();
		System.out.println(decryptText(cryptText("Hallo, ich bin Tobias", key), key));
	}

	public static char crypt(char c, char[] key) {
		c=capitalize(c);
		if(c >= 'A' & c <='Z') {
			c = (char) (key[c%'A']);
		}
		return c;
	}
	public static char decrypt(char c, char[] key) {
		c=capitalize(c);
		if(c >= 'A' & c <='Z') {
			for(int i=0; i<26; i++) {
				if(key[i] == c) {
					return (char) (i+'A');
				}
			}
		}
		return c;
	}
	public static String cryptText(String s, char[] key) {
		String buf="";
		for(int i = 0; i<s.length(); i++ ) {
			buf+=crypt(s.charAt(i), key);
		}
		return buf;
	}
	public static String decryptText(String s, char[] key) {
		String buf="";
		for(int i = 0; i<s.length(); i++ ) {
			buf+=decrypt(s.charAt(i), key);
		}
		return buf;
	}

	public static char capitalize(char c) {
		if(c >= 'a' & c <= 'z') {
			c = (char) (c - 32);
		}
		return c;
	}
}