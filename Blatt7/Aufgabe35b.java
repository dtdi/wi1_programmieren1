import static jsTools.Input.*;

public class Aufgabe35b {
	public static void main(String args[]) {
		char[] key, 
			histogramm=new char[26],
			rKey=new char[26];

		String ln;	    
	    // Ein File oeffnen, zum Lesen vorbereiten
	    fileReadOpen("Substitution.tab");
	    //eine Zeile lesen:
	    ln=fileReadLine();
	    System.out.println(ln);


	    // Zweite Zeile Lesen:
	    ln=fileReadLine();
	    System.out.println(ln);
	    key = ln.toCharArray();

		for(int i=0;i<rKey.length;i++) {
			rKey[key[i]%'A'] = (char) (i + 'A');
		}

	    fileReadOpen("geheim2.txt");
	    ln=fileReadLine();

	    char c;
	    long all=0;
	    while (ln != null) {
	    	for(int i=0; i<ln.length(); i++) {
	    		c=capitalize(ln.charAt(i));
				if(c >= 'A' & c <='Z') {
					histogramm[c % 'A']++;
					all++;
				}
	    	}
	    	System.out.println(decryptText(ln, rKey));
	    	ln=fileReadLine();
	    }


	    System.out.println("\n-------------------");
	    System.out.println("Haeufigkeitsanalyse");
	   	System.out.println("-------------------\n");

	    for(int i=0; i<histogramm.length; i++) {
	    	System.out.println((char) (i+'A') + " ("
	    		+ (char) (rKey[i]) + "):\t" 
	    		+ (int) histogramm[i] + "\t" 
	    		+ ((histogramm[i] * 100.0) / all) + " %");
	    }
	}

	public static char crypt(char c, char[] key) {
		c=capitalize(c);
		if(c >= 'A' & c <='Z') {
			c = (char) (key[c%'A']);
		}
		return c;
	}
	public static char decrypt(char c, char[] key) {
		c=capitalize(c);
		if(c >= 'A' & c <='Z') {
			c = (char) (key[c%'A']);
		}
		return c;
	}
	public static String cryptText(String s, char[] key) {
		String buf="";
		for(int i = 0; i<s.length(); i++ ) {
			buf+=crypt(s.charAt(i), key);
		}
		return buf;
	}
	public static String decryptText(String s, char[] key) {
		String buf="";
		for(int i = 0; i<s.length(); i++ ) {
			buf+=decrypt(s.charAt(i), key);
		}
		return buf;
	}

	public static char capitalize(char c) {
		if(c >= 'a' & c <= 'z') {
			c = (char) (c - 32);
		}
		return c;
	}
}