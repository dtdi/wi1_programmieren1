import static jsTools.Wand.*;

public class Wand1 {
	final static int SIZE = 24;
	public static void main(String args[]) {
		initWand(30);
		paintWall("#ff7300");

	}

	public static void paintWall(String color) {
		String[][] wall = new String[SIZE][SIZE];
		for(int i=0; i<SIZE;i++) {
			for(int j=0; j<SIZE;j++) {
				setWandColor(i,j,color);
			}
		}
		setWandColors(wall);
	}
}