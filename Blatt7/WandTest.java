import static jsTools.Wand.*;


public class WandTest {

  public static void bunteWand() {
    String bunt[][]=new String[24][24];
    for (int j=0;j<24;j++) {
      for (int i=0;i<24;i++) {
        bunt[i][j]=randColor();
      }
    }
    setWandColors(bunt);
  }

  public static boolean snowing(String [][] arr) {
    final int SNOWINTENSITY=3;

    boolean ganzWeiss=true;
    for (int j=arr.length-2;j>=0;j--) {
      for (int i=0;i<arr[0].length;i++) {

        if (arr[j+1][i].equals("black")) {
          arr[j+1][i]=arr[j][i];
          arr[j][i]="black";
          ganzWeiss=false;
        }
        ganzWeiss=false;
      }
    }
    //Schneeflocken erzeugen
    // Position durch Zufallszahl zwischen 0 und screenbreite (inkl), 
    // evtl wird eine Flocke mehrfach gesetzt
    for (int i=0;i<SNOWINTENSITY;i++) {
      arr[0][(int)(Math.random()*arr[0].length)]="white";
    }
    setWandColors(arr);

    return ganzWeiss;
  }

  public static void buildHouse(int y, int x, String screen[][]) {
    final int BREITE=6;
    final int HOEHE=3;

    for (int i=y; i>y-HOEHE; i--) {
      for (int j=x;j<x+BREITE;j++) {
        screen[i][j]="blue";
      }
    }
    // Dach
    for (int i=0;i<BREITE/2+1;i++)
      for (int j=x+i;j<x+BREITE-i;j++)
        screen[y-HOEHE-1-i][j]="red";
    // Türe
    screen[y][x+BREITE/2+1]="brown";
    screen[y-1][x+BREITE/2+1]="brown";
    // Fenster
    screen[y-1][x+BREITE/2-1]="yellow";
  }

  public static void snowFallLandscape() {
    String screen[][]=new String[24][24];
    for (int j=0;j<24;j++) {
      for (int i=0;i<24;i++) {
        screen[i][j]="black";
      }
    }
    buildHouse(23,14, screen);

    boolean ganzWeiss=false;
    while (!ganzWeiss) {
      ganzWeiss=snowing(screen);

      setWandColors(screen);
      //  while(flockenFallen(screen))
      sleep(100);
    }
  }

  public static void main(String args[]) {
    // Wand initialisieren
    initWand(30);

    // nur 2* 1 Pixel malen
    setWandColor(2,3,"green");
    setWandColor(4,5,"#341f77");
    paintWandNew();  // neuzeichnen nach setWandColor
    sleep(500);
    
    bunteWand();
    sleep(500);
    
    // ein bewegtes Motiv
    snowFallLandscape();
  }
}
