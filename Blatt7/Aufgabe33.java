import static jsTools.Input.*;

public class Aufgabe33 {
	public static void main(String args[]) {
		int length;
		do {
			length = readInt("Laenge eingeben: ");
		} while(length == 0);

		char[] caValue = new char[length];

		for(int i=0;i<length;i++) {
			do{
				caValue[i] = readChar("Buchstabe " + (i+1) + " eingeben: ");
			} while(caValue[i] == 0);
		}

		for(int i=0;i<length;i++) {
			System.out.print(caValue[i]);
		}
		System.out.println();

		char buf;
		for(int i=0; i<length/2;i++) {
			buf = caValue[i];
			caValue[i] = caValue[length-1-i];
			caValue[length-1-i] = buf;
		}

		for(int i=0;i<length;i++) {
			System.out.print(caValue[i]);
		}
		System.out.println();

	}
}