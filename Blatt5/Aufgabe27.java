public class Aufgabe27 {
	public static void main(String args[]) {
		prim();
	}

	public static void prim() {
		prim:
		for(int i=1;i<=1000;i++) {
			for(int j=2;j<i;j++) {
				if(i%j==0) {
					continue prim;
				}
			}
			System.out.print(i + ", ");
		}
	}

}