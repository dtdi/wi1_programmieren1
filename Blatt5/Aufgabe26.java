public class Aufgabe26 {
	public static void main(String args[]) {

		calcSeconds(987654321);
	}

	public static void calcSeconds(int seconds) {
		System.out.println(seconds/(365*24*60*60) + "\tJahre");
		seconds = seconds%(365*24*60*60);

		System.out.println(seconds/(24*60*60) + "\tTage");
		seconds = seconds%(24*60*60);

		System.out.println(seconds/(60*60) + "\tStunden");
		seconds = seconds%(60*60);

		System.out.println(seconds/(60) + "\tMinuten");
		seconds = seconds%(60);
		System.out.println(seconds + "\tSekunden");
	}

}