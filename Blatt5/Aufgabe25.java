import static jsTools.Input.*;


public class Aufgabe25 {
	public static void main(String args[]) {
		char c1 = readChar("c1 eingeben: ");
		char c2 = readChar("c1 eingeben: ");
		if(!isLetter(c1)) 
			System.out.println(c1 + " ist kein Buchstabe");
		if(!isLetter(c2)) 
			System.out.println(c2 + " ist kein Buchstabe");
		c1 = toSmall(c1);
		c2 = toSmall(c2);
		System.out.println("'" + c1 + "' " + "kleiner?\t" + "'" + c2 + "': " + (c1<c2));
		System.out.println("'" + c1 + "' " + "groesser?\t" + "'" + c2 + "': " + (c1>c2));
		System.out.println("'" + c1 + "' " + "gleich?\t" + "'" + c2 + "': " + (c1==c2));

	}

	public static boolean isLetter(char c) {
		if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
			return true;
		else 
			return false;
	}

	public static char toSmall(char c) {
		if(c >= 'A' && c <= 'Z')
			return (char) (c+32);
		else 
			return c;
	}
}