import static jsTools.Input.*;

public class CalcIBAN {

  //berechnet BBAN
  public static long calcBBAN(long blz, long ktoNr) {
    return blz*10000000000l+ktoNr;
  }

  public static char toBig(char c) {
    if('a' <= c && c <= 'z') {
      return (char) (c-32);
    } else {
      return c;
    }
  }

  public static long calcLandKennung(char land1, char land2) {
    land1=toBig(land1);
    land2=toBig(land2);
    int sub = -64 + 9;
    return (long) ((land1+sub)*10000 + (land2 + sub)*100);
  }

  public static String calcPruefziffer(char land1, char land2, long blz, long ktoNr) {
    long buf= 98 - ((calcBBAN(blz, ktoNr) % 97) * 1000000 
      + calcLandKennung(land1, land2)) % 97;

    if(buf < 10)
     return "0" + buf;
    else 
      return "" + buf;
  }

  public static String calcIBAN(char land1, char land2, long blz, long ktoNr) {
    return "" + toBig(land1) + toBig(land2) + calcPruefziffer(land1, land2, blz, ktoNr)
      + calcBBAN(blz, ktoNr);

  }

  public static void main(String args[]) {
    System.out.println();
    // pruefe Methoden:
    if ( 700901001234567890l != calcBBAN( 70090100, 1234567890))
      System.out.println ("Fehler: Berechnung BBAN:" +
                          " 700901001234567890l != calcBban( 70090100, 1234567890)):"+
                          calcBBAN( 70090100, 1234567890));
    else System.out.println("OK");

    if ( 'A' != toBig('A'))
      System.out.println ("Konvertierungsfehler: 'Z' != toBig('Z')");
    else System.out.println("OK");
    if ( 'A' != toBig('a'))
      System.out.println ("Konvertierungsfehler: 'A' != toBig('a')");
    else System.out.println("OK");
    if ( 'Z' != toBig('z'))
      System.out.println ("Konvertierungsfehler: 'Z' != toBig('z')");
    else System.out.println("OK");
    if ( 'K' != toBig('k'))
      System.out.println ("Konvertierungsfehler: 'K' != toBig('k')");
    else System.out.println("OK");

    if ( 131400 != calcLandKennung( 'd','e'))
      System.out.println ("Fehler: Berechnung Landkennung:" +
                          " 131400 != calcLandKennung( 'd','e') :"+
                          calcLandKennung( 'd','e'));
    else System.out.println("OK");

    if (! "08".equals( calcPruefziffer( 'd','e',70090100, 1234567890)))
      System.out.println ("Fehler: Berechnung Pruefziffer:" +
                          " 08 != calcPruefziffer( 'd','e',70090100, 1234567890):"+
                          calcPruefziffer( 'd','e',70090100, 1234567890));
    else System.out.println("OK");
    
 

    if (! "DE08700901001234567890".equals(calcIBAN('d','e',70090100, 1234567890)))
      System.out.println ("Fehler: Berechnung IBAN:"+
                          "DE08700901001234567890 ungleich"+
                          calcIBAN('d','e',70090100, 1234567890));
    else System.out.println("OK");

    char land1=readChar("Bitte geben Sie die erste Stelle der Laenderkennung ein:");
    char land2=readChar("Bitte geben Sie die zweite Stelle der Laenderkennung ein:");
    long blz=readInt("Bitte geben Sie die Bankleitzahl ein:");
    long kontoNr=readInt("Bitte geben Sie die Kontonummer ein:");

    // Hier folgt der Aufruf fuer die Berechnung: und die Ausgabe
    System.out.println("IBAN:"+calcIBAN('d','e',blz, kontoNr));
  }
}
