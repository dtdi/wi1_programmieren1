/* Hier benoetigen wir die jsTools */
import static jsTools.Input.*;

class Person{
  /* 
   * Attribute wie bisher:
   * 1)  Personennummer (pnr)
   * 2)  Namen (name)
   * 3)  Postleitzahl (zip)
   * 4)  Wohnort (ort)
   */
   private int pnr;
   private String name;
   private int zip;
   private String ort;

   /* 
    * Einen Zaehler, der 
    * bei jeder erzeugten Person
    * um 1 erhoeht wird
    */
    static int cnt;
   
   /* 
    * einen Konstruktor, der die Attribute
    * von der Konsole einliest. 
    * Die Personennummer soll den Wert des Zaehlers annehmen
    */
    public Person() {
      this(
        ++Person.cnt,
        readString("Name: "),
        readInt("Zip: "),
        readString("Ort: ")
      );
    }
    
    /*
     * einen Konstruktor, der alle 4 Parameter 
     * als Attribut übergeben bekommt
     */
     public Person(int pnr, String name, int zip, String ort) {
      this.pnr = pnr;
      this.name = name;
      this.zip = zip;
      this.ort = ort;
     }
   
   /* Uebersichtliche Ausgabe einer Instanz der Person
    *  mit allen Attributen                           
    */
   void printPerson(){
    System.out.println("Personennummer (pnr)\t" + this.pnr + "\n"
      + "Namen (name)\t\t" + this.name + "\n"
      + "Postleitzahl (zip)\t" + this.zip + "\n"
      + "Wohnort (ort)\t\t" + this.ort + "\n");
   }

   /* Einlesen der Attribute einer vorhandenen Instanz der Person
    * von der Konsole                           
    */
   void readPerson(){
      this.name = readString("Name: ");
      this.zip = readInt("Zip: ");
      this.ort =readString("Ort: ");
   }

   /* Ueberpruefen, ob Personennummer der Instanz
    *   gleich dem uebergebenen Parameter n ist  
    */
   boolean isPnr( int n){
     return (this.pnr == n ? true : false);
   }

}

class PersonenKartei{
  /* die Attribute dieser Klasse: 
   */

  /* ein Array von Personen 
   */
  Person kartei[];
  
  /* ein Zaehler, der sich merkt,
   * wieviele Personen bereits eingetragen sind  
   */
  // hier den Zaehler erstellen
  public int cnt=0;

  /* Konstuktor:
   * instanziiert das Array mit einer Groesse von 10
   * Elementen
   */
  public PersonenKartei() {
    this.kartei = new Person[11];
  }

  /* Die Person, die im Parameter übergeben wurde
   * in der Kartei einfügen
   */
  void addPerson(Person p){
    this.kartei[this.cnt++] = p;
  }
  
  /* Eine neue Person anlegen und
   * Parameter dieser Person-Instanz von der Konsole
   * einlesen                                         */
  void addNewPerson(){
    this.addPerson(new Person());
  }

  // Ausgeben aller Personen
  void printKartei(){
    for(int i=0; i<this.cnt;i++) {
      this.kartei[i].printPerson();
    }
  }

  /* Suchen einer Person mit der Personennummer n
   * und ausgeben der Personendaten (Attribute),
   * wenn diese Person da ist                      */
  void searchPerson(int n){
    for(int i=0; i<this.cnt;i++) {
      if(this.kartei[i].isPnr(n)) {
        this.kartei[i].printPerson();
      }
    }
  }

  /* 
   * Einlesen einer Personennummer und anschliessend
   * suchen nach der Person mit der eingegeben Personennummer
   * und ausgeben der Personendaten (Attribute),
   * falls diese Person vorhanden ist                        
   */
  void searchPerson(){
    this.searchPerson(readInt("Personennummer: "));
  }
}

public class MainPerson{

  /* jetzt kommt das main:
   */
  public static void main(String [] a){
    /* zuerst wird eine Instanz der PersonenKartei erstellt */
    PersonenKartei meineKartei=new PersonenKartei();

    /* dann wird ein (klassisches) Textmenue solange ausgegeben und
     * bearbeitet, bis die Eingabe fuer "beenden" erfolgt.
     * Folgende Optionen bietet das Menue an:
     *   Einfuegen, Ausgeben der Kartei,
     *   Suchen nach einer Person ueber deren Personennummer,
     *   Beenden.
     */
    String befehl = "";
    do {
      System.out.println("\nPersonenkartei.\n\n"
        + "Wählen Sie einen Befehl:\n"
        + "- 'einfuegen'\n"
        + "- 'ausgeben'\n"
        + "- 'suchen'\n"
        + "- 'beenden'\n\n"
        );
      befehl = readString("Befehl: ");
      switch(befehl) {
        case "einfuegen": meineKartei.addNewPerson(); break;
        case "ausgeben": meineKartei.printKartei(); break;
        case "suchen": meineKartei.searchPerson(); break;
        default: break;
      }
    } while(!befehl.equals("beenden"));


  }
}

