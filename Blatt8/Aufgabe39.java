import static jsTools.Input.*;


public class Aufgabe39 {
	public static void main(String args[]) {
		Auto auto1 = new Auto();
		auto1.readValues();
		auto1.print();

		Auto auto2 = new Auto();
		auto2.init("Mazda", 500, 500, 300, 1999);
		auto2.print();
		auto2.driveTo(400, 400);
		auto2.print();
	}
	
}

class Auto {
	private String hersteller;
	private int posX;
	private int posY;
	private double kmStand;
	private int baujahr;

	public void init(String hst, int x, int y, int km, int bj) {
		this.hersteller = hst;
		this.posX = x;
		this.posY = y;
		this.kmStand = (double) km;
		this.baujahr = bj;
	}

	public void readValues() {
		this.hersteller = readString("Hersteller: ");
		this.posX = readInt("Pos X: ");
		this.posY = readInt("Pos Y: ");
		this.kmStand = readInt("kmStand: ");
		this.baujahr = readInt("Baujahr: ");
	}

	public void driveTo(int xNeu, int yNeu) {
		this.kmStand += Math.sqrt(
			Math.pow(this.posX - xNeu, 2) + 
			Math.pow(this.posY - yNeu, 2));
		this.posX = xNeu;
		this.posY = yNeu;
	}

	public void print() {
		System.out.println(
			"Hersteller:\t" + this.hersteller 
			+ "\nposX:\t" + this.posX
			+ "\nposY:\t" + this.posY
			+ "\nkmStand:\t" + this.kmStand
			+ "\nBaujahr:\t" + this.baujahr
		);
	}
}