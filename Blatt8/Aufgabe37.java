import static jsTools.Input.*;

public class Aufgabe37 {
	public static void main(String args[]) {
		int num = readInt("Wie viele Personen? ");
		Person[] personen = new Person[num];
		for(int i=0; i<personen.length; i++) {
			personen[i] = new Person();
		}
		for(int i=0; i<personen.length; i++) {
			System.out.print(personen[i]);
		}

	}
}

class Person {
	public int pnr;
	public String name;
	public int zip;
	public String ort;

	public Person() {
		this(
			readInt("Personennummer: "), 
			readString("Name: "),
			readInt("Plz: "),
			readString("Ort: ")
		);
	}

	public Person(int pnr, String name, int zip, String ort) {
		this.pnr = pnr;
		this.name = name;
		this.zip = zip;
		this.ort = ort;
	}

	public String toString() {
		return "Personennummer (pnr)\t" + this.pnr + "\n"
			+ "Namen (name)\t\t" + this.name + "\n"
			+ "Postleitzahl (zip)\t" + this.zip + "\n"
			+ "Wohnort (ort)\t\t" + this.ort + "\n";
	}

}