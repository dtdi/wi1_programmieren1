public class Aufgabe38 {
	public static void main(String args[]) {
		int zahl = 234242344;
		teiler(zahl);
		System.out.println();
		primfaktor(zahl);
	}

	public static void teiler(int zahl) {
		int brk=0;
		for(int i=2; i<=zahl/2; i++) {
			if(zahl%i == 0) {
				System.out.print(i + "\t");
				brk++;
			}
			if(brk==4) {
				System.out.println();
				brk=0;
			}
		}
	}

	public static void primfaktor(int zahl) {
		prim:
		for(int i=2;i<=zahl;i++) {
			for(int j=2;j<i;j++) {
				if(i%j==0) {
					continue prim;
				}
			}
			if(zahl%i==0) {
				zahl = zahl / i;
				System.out.print(i + (zahl > i ? "*" : "\n"));
				i=1;
			}
		}
	}
}