import static jsTools.Input.*;


class Sudoku {
	public int[][] field = new int[9][9];

	public Sudoku(int[][] data) {
		this.field = data;
	}
	public Sudoku() {
		this.field[0][3] = 7;
		this.field[0][5] = 1;

		this.field[1][0] = 2;
		this.field[1][4] = 8;
		this.field[1][8] = 1;

		this.field[2][0] = 1;
		this.field[2][1] = 4;
		this.field[2][3] = 2;
		this.field[2][4] = 6;
		this.field[2][5] = 3;
		this.field[2][7] = 9;
		this.field[2][8] = 5;

		this.field[3][2] = 6;
		this.field[3][3] = 1;
		this.field[3][5] = 9;
		this.field[3][6] = 2;

		this.field[4][0] = 4;
		this.field[4][8] = 6;

		this.field[5][2] = 5;
		this.field[5][3] = 6;
		this.field[5][5] = 8;
		this.field[5][6] = 9;

		this.field[6][0] = 8;
		this.field[6][1] = 2;
		this.field[6][3] = 4;
		this.field[6][4] = 1;
		this.field[6][5] = 7;
		this.field[6][7] = 6;
		this.field[6][8] = 9;

		this.field[7][0] = 9;
		this.field[7][4] = 5;
		this.field[7][8] = 3;

		this.field[8][3] = 9;
		this.field[8][5] = 6;
	}

	public void print() {
		char x;
		for (int i = 0; i < this.field.length; i++) {
			if (i % 3 == 0) {
				System.out.println("=========================================");
			} else {
				System.out.println("-----------------------------------------");
			}
			System.out.print("|");
			for (int j = 0; j < this.field.length; j++) {
				if (j % 3 == 0) {
					System.out.print("|");
				}
				System.out.print(" " + (this.field[i][j] == 0 ? " " : this.field[i][j])
						+ " |");
				if (j == 8) {
					System.out.println("|");
				}
			}
		}
		System.out.println("=========================================");

	}

	public boolean[] testZeile(int zeile) {
		boolean[] row = new boolean[10];
		for(int i=1;i<10;i++){
			row[this.field[zeile][i-1]] = true;
		}
		return row;
	}

	public boolean[] testSpalte(int spalte) {
		boolean[] col = new boolean[10];
		for(int i=1;i<10;i++){
			col[this.field[i-1][spalte]] = true;
		}
		return col;
	}

	public boolean[] testBox(int zeile, int spalte) {
		boolean[] box = new boolean[10];
		int x = (int) Math.floor(zeile / 3) * 3;
		int y = (int) Math.floor(spalte / 3) * 3;

		for (int a = x; a < x + 3; a++) {
			for (int b = y; b < y + 3; b++) {
				box[this.field[a][b]] = true;
			}
		}
		return box;
	}

	public void testZelle() {
		int zeile = readInt("Zeile: ");
		int spalte = readInt("Spalte: ");
		int[] val = this.testZelle(zeile, spalte);
	}

	public int[] testZelle(int zeile, int spalte) {
		int[] values = new int[9];
		int[] fin;
		int cnt = 0;
		boolean[] row = testZeile(zeile);
		boolean[] col = testSpalte(spalte);
		boolean[] box = testBox(zeile, spalte);
		for(int i=1;i<=9;i++) {
			if(!row[i] && !col[i] && !box[i]) {
				values[cnt++] = i;
			}
		}
		fin = new int[cnt];
		for(int i=0;i<cnt;i++) {
			fin[i] = values[i];
		} 
		return fin;
	}

	public void setZelle(int zeile, int spalte, int val) {
		this.field[zeile][spalte] = val;
	}

	public void testAll() {
		for(int i=0;i<9;i++) {
			for(int j=0;j<9;j++) {
				if(this.field[i][j] == 0) {
					testZelle(i,j);
				}
			}
		}
	}
	public void solveSudoku() {
		int test[];
		for(int i=0;i<9;i++) {
			for(int j=0;j<9;j++) {
				if(this.field[i][j] == 0) {
					test = testZelle(i,j);
					if(test.length == 1) {
						this.setZelle(i,j,test[0]);
						System.out.println(i + " " + j + "=" + test[0]);
					}
				}
			}
		}
	}
}

public class SudokuSolver {
	public static void main(String args[]) {
		Sudoku sudoku = new Sudoku();
		sudoku.print();
		sudoku.solveSudoku();
		sudoku.print();
	}
}