/* Tobias Fehrer */

import static jsTools.Wand.*;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.io.File;
import java.util.Calendar;

public class Wand1 {
	final static int SIZE = 24;
	public static void main(String args[]) {
		new Wand1();
		paintWall("white");
		while(true) {
			clock();
			sleep(1000);
		}
	}

	public static void clock() {
		Calendar now = Calendar.getInstance();
		
		int h2 = now.get(Calendar.HOUR_OF_DAY);
		int h1=0;
		while (h2 > 9) {
        	h2 -= 10;
        	h1++;
    	}

    	int m2 = now.get(Calendar.MINUTE);
		int m1=0;
		while (m2 > 9) {
        	m2 -= 10;
        	m1++;
    	}
    	int s2 = now.get(Calendar.SECOND);
		int s1=0;
		while (s2 > 9) {
        	s2 -= 10;
        	s1++;
    	}
		paintWall("#" + h1 +h2 +m1+m2
			+ s1 +s2);

		String color = "black";
		if(m1 < 3 || h1 < 3 || s1 < 3) {
			color ="white";
		}
		writeDigit(h1,0, color);
		writeDigit(h2,1, color);
		writeDigit(m1,2, color);
		writeDigit(m2,3, color);

		color = randColor();
		setWandColor(9,11,color);
		setWandColor(9,12,color);
		setWandColor(13,11,color);
		setWandColor(13,12,color);
	}

	public static void paintWall(String color) {
		String[][] wall = new String[SIZE][SIZE];
		for(int i=0; i<SIZE;i++) {
			for(int j=0; j<SIZE;j++) {
				wall[i][j] = color;
			}
		}
		setWandColors(wall);
	}

	 public String printPixelARGB(int pixel) {
		Color c = new Color(pixel);
		String h = String.format("%02x%02x%02x", c.getRed(),c.getGreen(),c.getBlue());
		return h;
	}
 
	private void marchThroughImage(BufferedImage image) {
		int w = image.getWidth();
		int h = image.getHeight(); 
		String[][] wall = new String[SIZE][SIZE];
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				int pixel = image.getRGB(j, i);
				wall[i][j] = "#"+printPixelARGB(pixel);
			}
		}
		setWandColors(wall);
	}

	public static boolean[] toBinary(int number) {
		boolean[] bits = new boolean[5];
		for(int i = 6; i >= 0; i--) {
			bits[i] = (number & (1 << i)) != 0;
		}
		return bits;
	}

	/**
	 * 	Returns a Array representing a digit from 0-7
	 *  as a 5 * 7 boolean placeholder
	 *  
	 */
	public static boolean[][] getDigit(int d) {
		boolean[][] digit = new boolean[7][5];
		boolean c = true;

		/*
		 *		-xxx-
		 *		x---x
		 *		x---x
		 *		x---x
		 *		x---x
		 *		x---x
		 *		-xxx-
		 */

		if(d==0) {
			for(int i=2;i<5;i++) {
				digit[i][0] = c;
				digit[i][4] = c;
			}
		}
		if(d==1) {
			digit[2][0] = c;
			digit[1][1] = c;
			for(int i=0;i<7;i++) {
				digit[i][2] = c;
			}
			for(int i=0;i<5;i++) {
				digit[6][i]=c;
			}
		}
		if(d==0 | d==2 | d==3 | d == 6 | d == 8 | d== 9) {
			digit[1][0] = c;
			digit[0][1] = c;
			digit[0][2] = c;
			digit[0][3] = c;
			digit[1][4] = c;
		}
		if(d==2) {
			digit[2][4] = c;
			digit[3][1] = c;
			digit[3][2] = c;
			digit[3][3] = c;
			digit[4][0] = c;
			digit[5][0] = c;
			for(int i=0;i<5;i++) {
				digit[6][i]=c;
			}
		}
		if(d==0 | d==3 | d == 5 | d == 6 | d == 8 | d== 9) {
			digit[5][0] = c;
			digit[6][1] = c;
			digit[6][2] = c;
			digit[6][3] = c;
			digit[5][4] = c;
		}
		if(d==3) {
			digit[2][4] = c;
			digit[3][3] = c;
			digit[3][2] = c;
			digit[4][4] = c;
		}
		if(d==4) {
			for(int i=0;i<7;i++) {
				digit[i][3]=c;
			}
			for(int i=0;i<5;i++) {
				digit[4][i]=c;
			}
			digit[1][2]=c;
			digit[2][1]=c;
			digit[3][0]=c;
		}
		if(d==5) {
			for(int i=0;i<5;i++) {
				digit[0][i]=c;
			}
			for(int i=1;i<4;i++) {
				digit[i][0]=c;
			}
			for(int i=0;i<4;i++) {
				digit[3][i]=c;
			}
			digit[4][4]=c;
		}
		if(d==6) {
			for(int i=1;i<5;i++) {
				digit[i][0]=c;
			}
			digit[3][1]=c;
			digit[3][2]=c;
			digit[3][3]=c;
			digit[4][4]=c;
		}
		if(d==7) {
			for(int i=0;i<5;i++) {
				digit[0][i]=c;
			}
			for(int i=4;i<7;i++) {
				digit[i][1]=c;
			}
			digit[1][4]=c;
			digit[2][3]=c;
			digit[3][2]=c;
		}
		if(d==8) {
			digit[2][0]=c;
			digit[2][4]=c;
			digit[3][1]=c;
			digit[3][2]=c;
			digit[3][3]=c;
			digit[4][0]=c;
			digit[4][4]=c;
		}
		if(d==9) {
			digit[2][0]=c;
			digit[2][4]=c;
			digit[3][1]=c;
			digit[3][2]=c;
			digit[3][3]=c;
			digit[3][4]=c;
			digit[4][4]=c;
		}

		return digit;
		
	}

	public static void writeDigit(int d, int pos, String color) {
		boolean[][] digit = getDigit(d);
		for(int i=0;i<7;i++) {
			for(int j=0;j<5;j++) {
				if(digit[i][j]) {
					setWandColor(8+i,(pos < 2 ? pos*6 : pos*6+1)+ j,color);
				}
			}
		}
	}
 
	public Wand1() {
		try {
			// get the BufferedImage, using the ImageIO class
			initWand(24);
			File dir = new File("./img");
			BufferedImage image = null;
			if (dir.isDirectory()) { // make sure it's a directory
	            for (final File f : dir.listFiles()) {
	                image = ImageIO.read(f);
					sleep(1000);
					marchThroughImage(image);
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}