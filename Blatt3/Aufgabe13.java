public class Aufgabe13 {
	public static void main(String args[]) {
		int a = Integer.parseInt(javax.swing.JOptionPane.showInputDialog(null, "Zahl 1"));
		int b = Integer.parseInt(javax.swing.JOptionPane.showInputDialog(null, "Zahl 2"));
		calcAndPrint(a, b);

	}
	
	public static void calcAndPrint(int a, int b) {
		// Zahlen werden gerundet.
		System.out.println(a+b);
		System.out.println(a-b);
		System.out.println(a*b);
		System.out.println(a/b);
	}
}