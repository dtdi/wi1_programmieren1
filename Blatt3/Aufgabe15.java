public class Aufgabe15 {
	public static void main(String args[]) {
		int dayHigh=0;
		int year = dialogWithCheck(1900, 2100, "Jahr");
		int month = dialogWithCheck(1, 12, "Monat");
		switch(month) {
			case 1: dayHigh=31; break;
			case 2: dayHigh=28; break;
			case 3: dayHigh=31; break;
			case 4: dayHigh=30; break;
			case 5: dayHigh=31; break;
			case 6: dayHigh=30; break;
			case 7: dayHigh=31; break;
			case 8: dayHigh=31; break;
			case 9: dayHigh=30; break;
			case 10: dayHigh=31; break;
			case 11: dayHigh=30; break;
			case 12: dayHigh=31; break;
			default: dayHigh=0;
		}
		int day = dialogWithCheck(1, dayHigh, "Tag");
		
		System.out.println(day + "." + month + "." + year);
	}
	
	public static int dialogWithCheck(int low, int high, String text) {
		int num=0;
		do {
			num = Integer.parseInt(javax.swing.JOptionPane.showInputDialog(null, text));
		} while(num > high || num < low);
		return num;
	}
}