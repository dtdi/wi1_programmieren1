public class TypeCast {
	public static void main(String args[]) {
		int j = 9;
		int i = (int) (0.5 * ++j/2);
		System.out.println(B(2342.23, 4));

		System.out.println(add(2,8));
	}

	public static double B(double a, int n) {
		if(n==0) {return 1;}
		else {
			return (a*B(a,n-1))/(a*B(a,n-1)+n);
		}
	}

	public static int add(int m, int n) {
		if(m<n) {
			if(m%2==0) {
				m++;
			}
			return m + add(m+2,n);
		} else {
			return 0;
		}
	}
}