for (int i = 0; i < 9; i++) {
      if (i % 3 == 0) {
        System.out.println("|+===+===+===++===+===+===++===+===+===+|");
      } else {
        System.out.println("|+---+---+---++---+---+---++---+---+---+|");
      }
      System.out.print("|");
      for (int j = 0; j < 9; j++) {
        if (j % 3 == 0) {
          System.out.print("|");
        }
        System.out.print(" " + " " + " |");
        if (j == 8) {
          System.out.println("|");
        }
      }
    }
    System.out.println("|+===+===+===++===+===+===++===+===+===+|");
