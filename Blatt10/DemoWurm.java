
import static jsTools.Input.*;
import static jsTools.Graph.*;
public class DemoWurm {
  // zeichnen eines Wurms
  static void paintWorm(int x, int y, int laenge) {
    final int D=50, // Durchmesser
                V=10; // Versatz der Kreise

    for (int i=0;i<laenge-1;i++) {
      addCircleBorder(x,y,D,green);
      x+=V;
    }
    addCircleBorder(x,y,D,yellow);
  }

  // zeichnen eines Wurms, wobei ein Array, das die Elementnummern
  // der Elemente des Wurms zurückgeliefert werden
  static int[] addWormE(int x, int y, int laenge) {
    final int D=50, // Durchmesser
                V=10; // Versatz der Kreise
    int elements[]=new int[laenge];

    int i;
    for (i=0;i<laenge-1;i++) {
      elements[i]=addCircleBorder(x,y,D,red);
      x+=V;
    }
    elements[i]=addCircleBorder(x,y,D,yellow);
    return elements;
  }

  // im Array uebergeben Elemente um dx und dy verschieben
  static void move(int[] elements, int dx, int dy) {
    for (int i=0; i<elements.length; i++) {
      int x=getX(elements[i]),
            y=getY(elements[i]);
      setPos(elements[i],x+dx,y+dy);
      paintNew();
    }
  }

  public static void main(String args[]) {
    paintWorm(50,50,6);

    sleep(500);

    int k=addCircleBorder(100,100,30,yellow);
    sleep(200);
    deleteObj(k);  // loeschen des Kreises
    sleep(200);

    //Kreis Zeichnen und verschieben
    int posxy=100;
    k=addCircleBorder(posxy,posxy,30,yellow);
    for (posxy=120; posxy<400; posxy+=10) {
      sleep(100);
      setPos(k,posxy,posxy);
      paintNew();
      System.out.println("Kreis auf Position: "+posxy+" verschoben");
    }

    sleep(1500);

    int[] w2=addWormE(10,600,10);
    System.out.println("Wurm bewegt sich nach rechts");
    for (int i=0;i<20;i++) {
      sleep(300);
      move(w2,10,0);
    }
    for (int i=0;i<20;i++) {
      sleep(300);
      move(w2,10,10);
    }
    System.out.println("Ende");
  }
}
