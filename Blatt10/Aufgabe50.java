import static jsTools.Graph.*;

public class Aufgabe50 {
	public static void main(String args[]) {
		initWindow(600, 600, "Triangles");
		addRect(300, 500, 600);
	}	

	public static void addRect(int x, int y, int size) {
		if(size > 12) {
			addRect(x + size/2,       y, size/2);
			addRect(x - size/2,       y, size/2);
			addRect(x      , y - size/2, size/2);
		} else {
			int height = (int) ((size * Math.sqrt(3)) / 2.0);
			addTriangle(x, y, size, height, 0, "black");
		}
	}
}