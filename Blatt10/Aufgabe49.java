public class Aufgabe49 {
	public static void main(String args[]) {
		System.out.println((double) e(20));
	}

	public static double e(int n) {
		if(n==0) {
			return 1.0;
		} else {
			int buf=0;
			for(int i=1;i<=n;i++) {
				buf += i;
			}
			return 1.0/buf + e(n-1);
		}
	}
}