import static jsTools.Graph.*;

public class PaintH {
	public static void main(String args[]) {
		initWindow(1200, 1200, "Triangles");
		paintH(500,500,600,true);
	}
	public static void paintH(int x, int y, int h, boolean dir) {
		if(h<1) return;
		else {
			drawLine(x,y,h,dir);
			if(dir) {
				paintH(x,y+h/2,h/2,!dir);
				paintH(x,y-h/2,h/2,!dir);
			} else {
				paintH(x+h/2,y,h/2,!dir);
				paintH(x-h/2,y,h/2,!dir);
			}
 		}
	}
	public static void drawLine(int x, int y, int h, boolean dir) {
		int hh = h/2;
		if(!dir) {
			addLine(x-hh, y, x+hh, y, "black");
		} else {
			addLine(x, y-hh, x, y+hh, "black");
		}
	}
}